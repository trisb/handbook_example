# Example - ML model of Hirshfeld ratios

Tristan Bereau
Max Planck Institute for Polymer Research, Mainz, Germany,
bereau@mpip-mainz.mpg.de

Example ML model as illustrated in
```
Tristan Bereau, Data-driven methods in multiscale modeling of soft matter. 
Handbook of Materials Modeling: Methods: Theory and Modeling, 1-12 (2018).
```

To be used together with the IPML software
`https://gitlab.mpcdf.mpg.de/trisb/ipml`

See also
``` 
Tristan Bereau, Robert A. DiStasio Jr., Alexandre Tkatchenko, and
O. Anatole von Lilienfeld, Non-covalent interactions across organic
and biological subsets of chemical space: Physics-based potentials
parametrized from machine learning, J Chem Phys 148 (2018); see also
https://arxiv.org/abs/1710.05871
```

## Installation

Simply clone the IPML repository

```
git clone https://gitlab.mpcdf.mpg.de/trisb/ipml.git
```

and create a symbolic link of the `ipml` folder to this one
(`handbook_example`).  You can then run the notebook using either
`ipython notebook` or `jupyter notebook`.

